cmake_minimum_required(VERSION 2.8.2)

# By default, build in Release mode. Must appear before project() command
if ( NOT DEFINED CMAKE_BUILD_TYPE )
  set( CMAKE_BUILD_TYPE Release CACHE STRING "Build type" )
endif ()

project(openturns-superbuild)

include(ExternalProject)

set(ep_base "${CMAKE_BINARY_DIR}/ExternalProjects")
set_property(DIRECTORY PROPERTY EP_BASE ${ep_base})

option(BUILD_SHARED_LIBS "Build shared libraries" ON)

set(ep_install_dir "${ep_base}/Install")

if(CMAKE_EXTRA_GENERATOR)
  set(gen "${CMAKE_EXTRA_GENERATOR} - ${CMAKE_GENERATOR}")
else()
  set(gen "${CMAKE_GENERATOR}")
endif()

set(ep_common_args
  -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
  -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
)
set(ot_depends)

if(BUILD_SHARED_LIBS)
    set(lib_prefix ${CMAKE_SHARED_LIBRARY_PREFIX})
    set(lib_suffix ${CMAKE_SHARED_LIBRARY_SUFFIX})
else()
    set(lib_prefix ${CMAKE_STATIC_LIBRARY_PREFIX})
    set(lib_suffix ${CMAKE_STATIC_LIBRARY_SUFFIX})
endif()

option( WITH_BOOST     "Download, build and install Boost"     ON )
option( WITH_HMAT_OSS  "Download, build and install hmat-oss"  ON )
option( WITH_LIBXML2   "Download, build and install LibXML2"   ON )
option( WITH_MUPARSER  "Download, build and install muParser"  ON )
option( WITH_NLOPT     "Download, build and install NLopt"     ON )
option( WITH_OPENTURNS "Download, build and install OpenTURNS" ON )

if(CMAKE_GENERATOR MATCHES Makefile)
    option( WITH_TBB  "Download, build and install TBB" ON )
else()
    option( WITH_TBB  "Download, build and install TBB" OFF )
    set(TBB_INCLUDE_DIR "" CACHE PATH "Existing TBB include directory")
    set(TBB_LIBRARY "" CACHE FILEPATH "Existing TBB library")
endif()
if(MSVC)
    option( WITH_OPENBLAS "Download, build and install OpenBLAS" OFF )
    set(OPENBLAS_INCLUDE_DIR "" CACHE PATH "Existing OpenBLAS include directory")
    set(OPENBLAS_LIBRARY "" CACHE FILEPATH "Existing OpenBLAS library")
else()
    option( WITH_OPENBLAS "Download, build and install OpenBLAS" ON )
endif()

#  hmat-oss does not export their symbols in DLL on Windows.
#  It must be compiled statically.  Ditto for libxml2 and
#  muParser, it is simpler to build static libraries
set(alt_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
set(alt_lib_suffix ${lib_suffix})
if(MSVC)
    set(alt_BUILD_SHARED_LIBS OFF)
    set(alt_lib_suffix ${CMAKE_STATIC_LIBRARY_SUFFIX})
endif()

############################################################################
# MUPARSER
#

if(WITH_MUPARSER)
    set(proj muParser)
    ExternalProject_Add(${proj}
      URL https://github.com/beltoforion/muparser/archive/v2.2.5.zip
      URL_MD5 63eb1e609008cd5a5dc42cd414828edb
      DOWNLOAD_NAME muparser_v2_2_5.zip
      UPDATE_COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/${proj}.cmake/CMakeLists.txt ${ep_base}/Source/${proj}/CMakeLists.txt
      CMAKE_GENERATOR ${gen}
      CMAKE_GENERATOR_TOOLSET ${CMAKE_GENERATOR_TOOLSET}
      CMAKE_ARGS
        ${ep_common_args}
        "-DCMAKE_INSTALL_PREFIX:PATH=${ep_install_dir}/${proj}"
        "-DBUILD_SHARED_LIBS:BOOL=${alt_BUILD_SHARED_LIBS}"
    )

    set(MUPARSER_INCLUDE_DIR ${ep_install_dir}/${proj}/include)
    set(MUPARSER_LIBRARY ${ep_install_dir}/${proj}/lib/${lib_prefix}muparser${alt_lib_suffix})
    set(ot_depends ${ot_depends} ${proj})
endif()

#############################################################################
## LIBXML2
##
#

if(WITH_LIBXML2)
    set(proj libxml2)
    ExternalProject_Add(${proj}
      GIT_REPOSITORY http://git.gnome.org/browse/libxml2
      GIT_TAG v2.9.1
      UPDATE_COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/${proj}.cmake/CMakeLists.txt ${ep_base}/Source/${proj}/CMakeLists.txt
      CMAKE_GENERATOR ${gen}
      CMAKE_GENERATOR_TOOLSET ${CMAKE_GENERATOR_TOOLSET}
      CMAKE_ARGS
        ${ep_common_args}
        "-DCMAKE_INSTALL_PREFIX:PATH=${ep_install_dir}/${proj}"
        "-DBUILD_SHARED_LIBS:BOOL=${alt_BUILD_SHARED_LIBS}"
    )

    set(LIBXML2_INCLUDE_DIR ${ep_install_dir}/${proj}/include)
    set(LIBXML2_LIBRARY ${ep_install_dir}/${proj}/lib/libxml2${alt_lib_suffix})
    set(ot_depends ${ot_depends} ${proj})
endif()

#############################################################################
## TBB
##
#

if(WITH_TBB)
    set(proj tbb)
    if(NOT CMAKE_GENERATOR MATCHES Makefile)
        message(FATAL_ERROR "TBB can be compiled directly from this script only when using Makefiles; you must first download, compile and install TBB, then set TBB_LIBRARY and TBB_INCLUDE_DIR variables")
    endif()
    set(make_args)
    if(CMAKE_BUILD_TYPE STREQUAL Release)
        set(release_type release)
    else()
        set(release_type debug)
    endif()
    set(make_args ${make_args} cfg=${release_type})
    set(TBB_INTERFACE)
    if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
      set(TBB_INTERFACE ".2")
    endif()
    # Note: TBB only builds shared libraries, not static ones.
    ExternalProject_Add(${proj}
      URL https://www.threadingbuildingblocks.org/sites/default/files/software_releases/source/tbb43_20140724oss_src.tgz
      URL_MD5 0791e5fc7d11b27360080ea4521e32bb
      CONFIGURE_COMMAND ""
      BUILD_IN_SOURCE 1
      BUILD_COMMAND tbb_build_prefix=cmake_build make ${make_args}
      INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${ep_base}/Source/${proj}/include/tbb ${ep_install_dir}/${proj}/include/tbb
                   && ${CMAKE_COMMAND} -E make_directory ${ep_install_dir}/${proj}/lib
                   && ${CMAKE_COMMAND} -E copy ${ep_base}/Source/${proj}/build/cmake_build_${release_type}/${CMAKE_SHARED_LIBRARY_PREFIX}tbb${CMAKE_SHARED_LIBRARY_SUFFIX}${TBB_INTERFACE} ${ep_install_dir}/${proj}/lib/
    )
    set(make_args)

    set(TBB_INCLUDE_DIR ${ep_install_dir}/${proj}/include)
    set(TBB_LIBRARY ${ep_install_dir}/${proj}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}tbb${CMAKE_SHARED_LIBRARY_SUFFIX}${TBB_INTERFACE})
    set(ot_depends ${ot_depends} ${proj})
endif()

#############################################################################
## OPENBLAS
##
#
if(WITH_OPENBLAS)
    if(MSVC)
        message(FATAL_ERROR "OpenBLAS cannot be compiled with MS compiler; you must download OpenBLAS binaries and set OPENBLAS_LIBRARY and OPENBLAS_INCLUDE_DIR variables")
    endif()
    set(proj openblas)
    set(make_args)
    if(BUILD_SHARED_LIBS)
        set(make_args ${make_args} NO_STATIC=1)
    else()
        set(make_args ${make_args} NO_SHARED=1)
    endif()
    if(NOT CMAKE_BUILD_TYPE STREQUAL Release)
        set(make_args ${make_args} DEBUG=1)
    endif()
    if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
        set(make_args ${make_args} NO_AFFINITY=1)
        if(CMAKE_SIZEOF_VOID_P MATCHES "8")
            if(BUILD_SHARED_LIBS)
                set(make_args ${make_args} "FCOMMON_OPT=-frecursive -fPIC")
            else()
                set(make_args ${make_args} FCOMMON_OPT=-frecursive)
            endif()
        endif()
    endif()
    ExternalProject_Add(${proj}
      URL http://github.com/xianyi/OpenBLAS/tarball/v0.2.19
      DOWNLOAD_NAME openblas_v0.2.19.tar.gz
      URL_MD5 267ee87caae46a877ec0609d62880317
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ""
      BUILD_COMMAND make ${make_args} USE_THREAD=1 NUM_THREADS=32 USE_OPENMP=0 DYNAMIC_ARCH=1
      INSTALL_COMMAND PREFIX=${ep_install_dir}/${proj} make -f Makefile.install install ${make_args}
    )
    set(make_args)

    set(OPENBLAS_LIBRARY ${ep_install_dir}/${proj}/lib/${lib_prefix}openblas${lib_suffix})
    set(OPENBLAS_INCLUDE_DIR ${ep_install_dir}/${proj}/include)
    set(ot_depends ${ot_depends} ${proj})

endif()

#############################################################################
## BOOST
##
#

if(WITH_BOOST)
    set(proj Boost)
    if(WIN32)
        set(cmd_suffix bat)
        set(exe_suffix .exe)
    else()
        set(cmd_suffix sh)
        set(exe_suffix)
    endif()
    file(TO_NATIVE_PATH ./bootstrap.${cmd_suffix} cmd_bootstrap)
    file(TO_NATIVE_PATH ./b2${exe_suffix} cmd_b2)
    set(b2_args threading=multi)
    if(BUILD_SHARED_LIBS)
        set(b2_args ${b2_args} link=shared)
    else()
        set(b2_args ${b2_args} link=static)
    endif()
    if(CMAKE_BUILD_TYPE STREQUAL Release)
        set(b2_args ${b2_args} variant=release)
    else()
        set(b2_args ${b2_args} variant=debug)
    endif()
    if(CMAKE_SIZEOF_VOID_P MATCHES "8")
        set(b2_args ${b2_args} address-model=64)
    else()
        set(b2_args ${b2_args} address-model=32)
    endif()
    ExternalProject_Add(${proj}
      #  sourceforge.net blocks downloads from France, use a mirror
      URL http://sourceforge.jp/frs/g_redir.php?f=/boost/boost/1.58.0/boost_1_58_0.tar.bz2
      URL_MD5 b8839650e61e9c1c0a89f371dd475546
      DOWNLOAD_NAME boost_1_58_0.tar.bz2
      BUILD_IN_SOURCE 1
      CONFIGURE_COMMAND ${cmd_bootstrap}
      BUILD_COMMAND ${cmd_b2} -q -d2 ${b2_args} install --prefix=${ep_install_dir}/${proj} --layout=system --with-math
      INSTALL_COMMAND ""
    )
    set(b2_args)

    set(BOOST_ROOT "${ep_install_dir}/${proj}")
    set(ot_depends ${ot_depends} ${proj})

endif()

#############################################################################
## HMAT
##
#

if(WITH_HMAT_OSS)
    set(proj hmat-oss)
    set(hmat_depends)
    if(WITH_OPENBLAS)
        set(hmat_depends openblas)
    endif()
    ExternalProject_Add(${proj}
      GIT_REPOSITORY https://github.com/jeromerobert/hmat-oss.git
      GIT_TAG hmat-oss-1.2
      CMAKE_ARGS
        ${ep_common_args}
        "-DCMAKE_INSTALL_PREFIX:PATH=${ep_install_dir}/${proj}"
        "-DBUILD_SHARED_LIBS:BOOL=${alt_BUILD_SHARED_LIBS}"
        "-DHMAT_GIT_VERSION:BOOL=OFF"
        "-DHMAT_VERSION:STRING=1.2.0"
        "-DHMAT_NO_VERSION:BOOL=ON"
        "-DUSE_DEBIAN_OPENBLAS:BOOL=OFF"
        "-DBLAS_FOUND:BOOL=ON"
        "-DBLAS_LIBRARIES:INTERNAL=${OPENBLAS_LIBRARY}"
        "-DLAPACK_FOUND:BOOL=ON"
        "-DLAPACK_LIBRARIES:INTERNAL=${OPENBLAS_LIBRARY}"
        "-DCBLAS_INCLUDE_DIRS:INTERNAL=${OPENBLAS_INCLUDE_DIR}"
      DEPENDS
        ${hmat_depends}
    )
    set(hmat_depends)
    set(HMAT_DIR "${ep_install_dir}/${proj}/lib/cmake/hmat")
    set(ot_depends ${ot_depends} ${proj})
endif()

#############################################################################
## NLOPT
##
#

if(WITH_NLOPT)
    set(proj nlopt)
    ExternalProject_Add(${proj}
      GIT_REPOSITORY https://github.com/stevengj/nlopt.git
      GIT_TAG master
      CMAKE_ARGS
        ${ep_common_args}
        "-DCMAKE_INSTALL_PREFIX:PATH=${ep_install_dir}/${proj}"
        "-DBUILD_SHARED_LIBS:BOOL=${alt_BUILD_SHARED_LIBS}"
    )
    set(NLOPT_INCLUDE_DIR ${ep_install_dir}/${proj}/include)
    set(NLOPT_LIBRARY ${ep_install_dir}/${proj}/lib/${lib_prefix}nlopt${alt_lib_suffix})
    set(ot_depends ${ot_depends} ${proj})
endif()

#############################################################################
## OPENTURNS
##

if(WITH_OPENTURNS)
    set(proj openturns)
    set(extra_cmake_args)
    if (MSVC)
        set(TBB_NO_IMPLICIT_LINKAGE TRUE)
        # See https://sourceware.org/bugzilla/show_bug.cgi?id=12633
        # We must add /OPT:NOREF in Release builds, otherwise OT crashes
        # when loading OpenBLAS dll.
        set(extra_cmake_args "-DCMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=/OPT:NOREF /INCREMENTAL:NO")
    endif()
    ExternalProject_Add(${proj}
      # For development version, use the corresponding branch or github fork
      # For a release, use the corresponding tag
      # github does not requies use of mirror as sourceforge
      GIT_REPOSITORY https://github.com/openturns/openturns.git
      GIT_TAG v1.8rc2
      CMAKE_ARGS
        ${ep_common_args}
        "-DCMAKE_INSTALL_PREFIX:PATH=${ep_install_dir}/${proj}"
        "-DUSE_LIBXML2:BOOL=ON"
        "-DLIBXML2_FOUND:BOOL=ON"
        "-DLIBXML2_INCLUDE_DIR:PATH=${LIBXML2_INCLUDE_DIR}"
        "-DLIBXML2_LIBRARIES:FILEPATH=${LIBXML2_LIBRARY}"
        "-DUSE_MUPARSER:BOOL=ON"
        "-DMUPARSER_FOUND:BOOL=ON"
        "-DMUPARSER_INCLUDE_DIR:PATH=${MUPARSER_INCLUDE_DIR}"
        "-DMUPARSER_LIBRARY:FILEPATH=${MUPARSER_LIBRARY}"
        "-DUSE_TBB:BOOL=OFF"
        "-DTBB_FOUND:BOOL=ON"
        "-DTBB_INCLUDE_DIRS:PATH=${TBB_INCLUDE_DIR}"
        "-DTBB_LIBRARIES:FILEPATH=${TBB_LIBRARY}"
        "-DOPENTURNS_TBB_NO_IMPLICIT_LINKAGE:BOOL=${TBB_NO_IMPLICIT_LINKAGE}"
        "-DBOOST_ROOT:FILEPATH=${BOOST_ROOT}"
        "-DUSE_LAPACKE:BOOL=OFF"
        "-DLAPACKE_FOUND:BOOL=ON"
        "-DOPENTURNS_LIBRARIES:FILEPATH=${OPENBLAS_LIBRARY}"
        "-DBUILD_PYTHON:BOOL=${alt_BUILD_SHARED_LIBS}"
        "-DUSE_HMAT:BOOL=ON"
        "-DHMAT_DIR:PATH=${HMAT_DIR}"
        "-DUSE_NLOPT:BOOL=ON"
        "-DNLOPT_FOUND:BOOL=ON"
        "-DNLOPT_INCLUDE_DIR:PATH=${NLOPT_INCLUDE_DIR}"
        "-DNLOPT_LIBRARY:FILEPATH=${NLOPT_LIBRARY}"
        ${extra_cmake_args}
      DEPENDS
        ${ot_depends}
    )
    set(extra_cmake_args)
endif()

